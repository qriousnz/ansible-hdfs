ansible-hdfs
=========

Ansible HDFS role

Requirements
------------

- Ansible 2.5.2 or newer
- a /data folder on the destination server


Role Variables
--------------
`namenodes`: a list of namenodes
`datanodes`: a list of datanodes
`hdfs_role`: datanode or namenode
`hdfs_bind_address`: The IP address to bind to

The configuration below is only required is HDFS authentication is required

`hostname`: the hostname of the server to be used on the kerberos configuration
`kerberos_realm_name`: name of the kerberos realm configured
`hdfs_keytab`: path to hdfs_keytab when required
